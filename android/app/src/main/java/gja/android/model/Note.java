package gja.android.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;

import java.util.ArrayList;

public class Note implements Parcelable {

    @SerializedName("id")
    private String mId;

    @SerializedName("title")
    private String mTitle;

    @SerializedName("text")
    private String mText;

    @SerializedName("tags")
    private ArrayList<String> mTags = new ArrayList<>();

    @SerializedName("attachments")
    public ArrayList<Attachment> mAttachments = new ArrayList<>();

    @SerializedName("comments")
    public ArrayList<Comment> mComments = new ArrayList<>();

    public Note()
    {}

    public String getId() {
        return mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getText() {
        return mText;
    }

    public void setText(String text) {
        mText = text;
    }

    public ArrayList<String> getTags() {
        return mTags;
    }

    public ArrayList<Attachment> getAttachments() {
        return mAttachments;
    }

    public ArrayList<Comment> getComments() {
        return mComments;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mId);
        dest.writeString(this.mTitle);
        dest.writeString(this.mText);
        dest.writeList(this.mTags);
        dest.writeTypedList(this.mAttachments);
        dest.writeTypedList(this.mComments);
    }

    protected Note(Parcel in) {
        this.mId = in.readString();
        this.mTitle = in.readString();
        this.mText = in.readString();
        this.mTags = in.readArrayList(String.class.getClassLoader());
        this.mAttachments = in.createTypedArrayList(Attachment.CREATOR);
        this.mComments = in.createTypedArrayList(Comment.CREATOR);
    }

    public static final Parcelable.Creator<Note> CREATOR = new Parcelable.Creator<Note>() {
        @Override
        public Note createFromParcel(Parcel source) {
            return new Note(source);
        }

        @Override
        public Note[] newArray(int size) {
            return new Note[size];
        }
    };
}
