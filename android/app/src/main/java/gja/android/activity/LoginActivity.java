package gja.android.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import gja.android.DataManager;
import gja.android.R;
import gja.android.UserManager;
import gja.android.model.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity
{
    @Bind(R.id.root)
    CoordinatorLayout mRootLayout;

    @Bind(R.id.loginForm)
    LinearLayout mLoginForm;

    @Bind(R.id.username)
    EditText mUsername;

    @Bind(R.id.password)
    EditText mPassword;

    @Bind(R.id.signIn)
    Button mSignIn;

    @Bind(R.id.register)
    TextView mRegister;

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);

        onCreate(savedInstanceState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);

        mSignIn.setOnClickListener(mSignInListener);
        mRegister.setOnClickListener(mRegisterListener);

        if(UserManager.getInstance(this).getAuthToken() == null)
        {
           mLoginForm.setVisibility(View.VISIBLE);
        }
        else
        {
            getUserInfo();
        }
    }

    private void getUserInfo()
    {
        DataManager.getInstance(this).getUserInfo(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {

                if(!response.isSuccessful())
                {
                    onFailure(call, null);
                    return;
                }

                login(response.body());
            }

            @Override
            public void onFailure(Call<User> call, Throwable t)
            {
                mLoginForm.setVisibility(View.VISIBLE);
            }
        });
    }

    private View.OnClickListener mSignInListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            final User u = new User();
            u.setName(mUsername.getText().toString());
            u.setPassword(mPassword.getText().toString());

            DataManager.getInstance(LoginActivity.this).login(u, new Callback<Void>()
            {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    login(u);
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {

                    Snackbar.make(mRootLayout, R.string.error_loginFailed, Snackbar.LENGTH_SHORT)
                            .show();
                }
            });
        }
    };


    private View.OnClickListener mRegisterListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Uri registerUri = DataManager.registerUserUri();

            Intent i = new Intent(Intent.ACTION_VIEW, registerUri);
            startActivity(i);
        }
    };

    private void login(User user)
    {
        UserManager.getInstance(this).reloginUser(user);

        Intent i = new Intent(LoginActivity.this, NotesActivity.class);
        startActivity(i);
        finish();
    }
}
