package gja.android.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SlidingPaneLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import android.widget.Toolbar;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import gja.android.R;
import gja.android.UserManager;
import gja.android.fragment.CommentsFragment;
import gja.android.fragment.NoteFragment;
import gja.android.fragment.NoteListFragment;
import gja.android.model.Comment;
import gja.android.model.Note;
import gja.android.view.UserMenuHeader;

public class NotesActivity extends AppCompatActivity implements NoteListFragment.NoteListHandler, NoteFragment.NoteHandler
{
    public static final String NOTE_FRAGMENT_TAG = "NoteFragmentTag";

    public static final int ADD_IMAGE_RESULT = 1;

    @Nullable
    @Bind(R.id.slidingPane)
    SlidingPaneLayout mSlidingPaneLayout;

    @Bind(R.id.drawerLayout)
    DrawerLayout mDrawerLayout;

    @Bind(R.id.navigationDrawer)
    NavigationView mNavigationView;

    private NoteFragment mNoteFragment;
    private NoteListFragment mNoteListFragment;

    private ActionBarDrawerToggle mDrawerToggle;

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);

        onCreate(savedInstanceState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(gja.android.R.layout.activity_main);

        ButterKnife.bind(this);

        FragmentManager fm = getSupportFragmentManager();

        mNoteListFragment = (NoteListFragment) fm.findFragmentByTag("noteList");
        mNoteFragment  = (NoteFragment) fm.findFragmentByTag("note");

        if(mNoteListFragment == null)
        {
            mNoteListFragment = new NoteListFragment();

            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.fragmentNoteList, mNoteListFragment, "noteList");

            ft.commit();
        }

        initMenu();
    }

    void initMenu()
    {
        mNavigationView.setNavigationItemSelectedListener(mMenuItemListener);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.openMenu, R.string.closeMenu)
        {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };

        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        mNavigationView.addHeaderView(new UserMenuHeader(this));
    }

    @Override
    protected void onStart() {
        super.onStart();

        if(mSlidingPaneLayout != null)
        {
            mSlidingPaneLayout.closePane();
            mSlidingPaneLayout.openPane();
        }
    }

    NavigationView.OnNavigationItemSelectedListener mMenuItemListener = new NavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(MenuItem menuItem) {

            if (menuItem.isChecked())
                menuItem.setChecked(false);
            else
                menuItem.setChecked(true);

            mDrawerLayout.closeDrawers();

            switch (menuItem.getItemId()) {
                case R.id.logout:
                    logout();
                    finish();
                    return true;

                default:
                    return true;
            }
        }
    };

    @Override
    public void onNoteSelected(Note note) {

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        mNoteFragment = new NoteFragment();
        mNoteFragment.setArguments(NoteFragment.createArgs(note));

        if(mSlidingPaneLayout != null)
        {
            ft.replace(R.id.fragmentNote, mNoteFragment, "note");

            mSlidingPaneLayout.closePane();
        }
        else
        {
            ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right, android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            ft.replace(R.id.fragmentNoteList, mNoteFragment);
            ft.addToBackStack(NOTE_FRAGMENT_TAG);
        }

        ft.commit();
    }

    @Override
    public Note onAddNote() {
        Note note = new Note();

        note.setTitle("Title");
        note.setText("<i>text</i>");

        onNoteSelected(note);

        return note;
    }

    @Override
    public void onNoteSaved(Note note, boolean exit)
    {
        FragmentManager fm = getSupportFragmentManager();

        mNoteListFragment.loadNotes();

        if(exit) {
            if (mSlidingPaneLayout != null) {
                mSlidingPaneLayout.closePane();
            } else {
                fm.popBackStack();
            }
        }
    }

    @Override
    public void onNoteComments(Note note) {
        Bundle args = CommentsFragment.createArgs(note);

        CommentsFragment fragment = new CommentsFragment();
        fragment.setArguments(args);

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        if(mSlidingPaneLayout != null)
        {
            fragment.show(fm, "coments");
        }
        else
        {
            ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right, android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            ft.replace(R.id.fragmentNoteList, fragment);
            ft.addToBackStack(NOTE_FRAGMENT_TAG);


            ft.commit();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        mDrawerToggle.onOptionsItemSelected(item);

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK)
        {
            if (data == null)
            {
                return;
            }

            try
            {
                InputStream inputStream = getContentResolver().openInputStream(data.getData());

                mNoteFragment.addFile(inputStream);
            }
            catch (IOException ex)
            {}
        }
    }

    private void logout()
    {
        UserManager.getInstance(this).logout();

        Intent i = new Intent(this, LoginActivity.class);
        startActivity(i);
    }
}
