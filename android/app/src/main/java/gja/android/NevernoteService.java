package gja.android;

import java.util.List;

import gja.android.model.Note;
import gja.android.model.User;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface NevernoteService
{
    @GET("note/list")
    Call<List<Note>> notes();

    @POST("note/add")
    Call<Void> addNote(@Body Note note);

    @POST("note/save")
    Call<Void> saveNote(@Body Note note);

    @GET("note/remove")
    Call<Void> removeNote(@Query("id") String id);


    @GET("note/comment")
    Call<Void> postComent(@Query("noteId") String noteId, @Query("text") String text);

    @Multipart
    @POST("note/upload")
    Call<Void> uploadNoteFile(@Part("file") RequestBody file,
                @Part("id") String description);

    @POST("note/removeAttachment")
    Call<Void> removeNoteFile(@Query("id") String id, @Query("path") String filePath);

    @POST("user/login")
    Call<Void> login(@Body User user);

    @GET("user/info")
    Call<User> userInfo();
}
