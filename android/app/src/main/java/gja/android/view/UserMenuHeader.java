package gja.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import gja.android.R;
import gja.android.UserManager;
import gja.android.model.User;

public class UserMenuHeader extends RelativeLayout
{
    @Bind(R.id.username)
    TextView mUsername;

    public UserMenuHeader(Context context) {
        super(context);

        init();
    }

    public UserMenuHeader(Context context, AttributeSet attrs) {
        super(context, attrs);

        init();
    }

    public UserMenuHeader(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init();
    }

    private void init()
    {
        inflate(getContext(), R.layout.menu_header, this);

        ButterKnife.bind(this);

        User user = UserManager.getInstance(getContext()).getLoggerUser();

        mUsername.setText(user.getName());
    }
}
