package gja.android;


import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.IntegerRes;
import android.support.annotation.RawRes;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Utils
{
    public static String htmlEndlToString(String text)
    {
        return text.replace("<br/>", "\n");
    }

    public static String stringEndlToHtml(String text)
    {
        return text.replace("\n", "<br/>");
    }

    public static String stringEndToSpace(String text)
    {
        return text.replace("\n", " ");
    }

    public static String loadRawFile(Context ctx, @RawRes int resId)
    {
        InputStream inputStream = ctx.getResources().openRawResource(resId);

        InputStreamReader inputReader = new InputStreamReader(inputStream);
        BufferedReader buffReader = new BufferedReader(inputReader);
        String line;
        StringBuilder text = new StringBuilder();

        try
        {
            while (( line = buffReader.readLine()) != null)
            {
                text.append(line);
                text.append('\n');
            }
        }
        catch (IOException e)
        {
            return null;
        }
        finally
        {
            try
            {
                buffReader.close();
            }
            catch(IOException e) {}
        }

        return text.toString();
    }

    public static byte[] getBytes(InputStream is) throws IOException {

        int len;
        int size = 1024;
        byte[] buf;

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        buf = new byte[size];
        while ((len = is.read(buf, 0, size)) != -1)
            bos.write(buf, 0, len);

        buf = bos.toByteArray();

        return buf;
    }

}
