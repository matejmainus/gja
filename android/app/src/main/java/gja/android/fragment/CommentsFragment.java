package gja.android.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.Bind;
import butterknife.ButterKnife;
import gja.android.DataManager;
import gja.android.R;
import gja.android.UserManager;
import gja.android.adapter.CommentAdapter;
import gja.android.model.Comment;
import gja.android.model.Note;
import gja.android.model.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CommentsFragment extends DialogFragment
{
    public static final String ARG_NOTE = "note";

    @Bind(R.id.list)
    RecyclerView mList;

    @Bind(R.id.text)
    EditText mText;

    @Bind(R.id.post)
    Button mPost;

    private Note mNote;

    private CommentAdapter mAdapter = new CommentAdapter();

    public static Bundle createArgs(Note note)
    {
        Bundle args = new Bundle();
        args.putParcelable(ARG_NOTE, note);

        return args;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();

        mNote = args.getParcelable(ARG_NOTE);

        mAdapter.setComments(mNote.getComments());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_comments, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);

        mList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        mList.setAdapter(mAdapter);

        mPost.setOnClickListener(mPostClickListener);
    }

    View.OnClickListener mPostClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            User user = UserManager.getInstance(getActivity()).getLoggerUser();

            final Comment comment = new Comment();
            comment.setText(mText.getText().toString());
            comment.setUsername(user.getName());
            comment.setUserId(user.getId());
            comment.setTimestamp(Calendar.getInstance().getTimeInMillis());

            DataManager.getInstance(getActivity()).postComent(mNote, comment, new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    if (!response.isSuccessful()) {
                        onFailure(call, null);
                        return;
                    }

                    mAdapter.addComment(comment);

                    mText.setText("");
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    Snackbar.make(getActivity().findViewById(android.R.id.content), R.string.error_commentPosted, Snackbar.LENGTH_SHORT)
                            .show();
                }
            });
        }
    };
}
