package gja.android.fragment;

import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import co.hkm.soltag.TagContainerLayout;
import co.hkm.soltag.TagView;
import gja.android.DataManager;
import gja.android.R;
import gja.android.Utils;
import gja.android.activity.NotesActivity;
import gja.android.adapter.NoteAdapter;
import gja.android.model.Attachment;
import gja.android.model.Note;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NoteFragment extends Fragment {

    private static final String ARG_NOTE = "note";

    @Bind(R.id.title)
    EditText mTitle;

    @Bind(R.id.text)
    WebView mText;

    @Bind(R.id.tags)
    TagContainerLayout mTags;

    @Bind(R.id.addImage)
    ImageButton mAddImage;

    @Bind(R.id.textEdit)
    EditText mTextEdit;

    @Bind(R.id.attachments)
    LinearLayout mAttachments;

    @Bind(R.id.comments)
    TextView mComments;

    private Note mNote;

    public static Bundle createArgs(Note note)
    {
        Bundle args = new Bundle();
        args.putParcelable(ARG_NOTE, note);

        return args;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();

        mNote = args.getParcelable(ARG_NOTE);
    }

    @Override
    public void onPause() {
        super.onPause();

        setHasOptionsMenu(false);
    }

    @Override
    public void onResume() {
        super.onResume();

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(gja.android.R.layout.fragment_note, container, false);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        ButterKnife.bind(this, view);

        mTitle.setText(mNote.getTitle());

        mText.setOnFocusChangeListener(mTextFocusListener);
        mTextEdit.setOnFocusChangeListener(mTextFocusListener);

        mTags.setOnTagClickListener(new TagView.OnTagClickListener() {

            @Override
            public void onTagClick(int position, String text) {
                if (position == mNote.getTags().size())
                    addTag();

            }

            @Override
            public void onTagLongClick(int position, String text) {

                if(position < mNote.getTags().size()) {
                    mNote.getTags().remove(position);

                    refreshTags();
                }
            }
        });

        refreshTags();

        reloadTextEditView();
        reloadTextView();

        mTextEdit.setBackgroundColor(Color.TRANSPARENT);
        mTextEdit.setVisibility(View.GONE);

        mAddImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                startActivityForResult(intent, NotesActivity.ADD_IMAGE_RESULT);
            }
        });

        injectAttachments();

        mComments.setText(getString(R.string.showComments, mNote.getComments().size()));
        mComments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getActivity() instanceof NoteHandler)
                {
                    NoteHandler handler = (NoteHandler) getActivity();
                    handler.onNoteComments(mNote);
                }
            }
        });
    }

    private View.OnFocusChangeListener mTextFocusListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if(v == mText)
            {
                if(hasFocus) {
                    mTextEdit.setVisibility(View.VISIBLE);
                    mText.setVisibility(View.GONE);

                    mTextEdit.requestFocus();
                    mTextEdit.requestFocusFromTouch();
                    mTextEdit.setSelection(0);

                    reloadTextEditView();
                }
            }
            else if(v == mTextEdit)
            {
                if(!hasFocus)
                {
                    mText.setVisibility(View.VISIBLE);
                    mTextEdit.setVisibility(View.GONE);

                    reloadTextView();
                }
            }
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId())
        {
            case R.id.save:
                saveNote();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.note_menu, menu);
    }

    private void reloadTextEditView()
    {
        if(mNote.getText() != null)
        {
            String text = Utils.htmlEndlToString(mNote.getText());

            mTextEdit.setText(text);
        }
        else
            mTextEdit.setText("");
    }

    public void addFile(InputStream is) throws IOException
    {
        DataManager.getInstance(getActivity()).uploadNoteFile(is, mNote, new Callback<Void>() {

            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (!response.isSuccessful()) {
                    onFailure(call, null);
                    return;
                }

                Snackbar.make(getActivity().findViewById(android.R.id.content), R.string.fileUploaded, Snackbar.LENGTH_SHORT)
                        .show();

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Snackbar.make(getActivity().findViewById(android.R.id.content), R.string.error_fileUpload, Snackbar.LENGTH_SHORT)
                        .show();

            }
        });
    }

    public void addTag()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.addTag);

        final EditText input = new EditText(getActivity());
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        builder.setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mNote.getTags().add(input.getText().toString());

                refreshTags();
            }
        });

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();

    }

    private void reloadTextView()
    {
        String text = Utils.stringEndlToHtml(mTextEdit.getText().toString());

        String htmlPage = Utils.loadRawFile(getActivity(), R.raw.note_text);

        mText.loadDataWithBaseURL("", htmlPage.replace("__text__", text), "text/html", "UTF-8", "");
    }

    private void saveNote()
    {
        mNote.setTitle(mTitle.getText().toString());

        String text = Utils.stringEndlToHtml(mTextEdit.getText().toString());
        mNote.setText(text);

        DataManager.getInstance(getActivity()).saveNote(mNote, new Callback<Void>() {

            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (!response.isSuccessful()) {
                    onFailure(call, null);
                    return;
                }

                if (getActivity() instanceof NoteHandler) {
                    NoteHandler handler = (NoteHandler) getActivity();
                    handler.onNoteSaved(mNote, true);
                }

                Snackbar.make(getActivity().findViewById(android.R.id.content), R.string.noteSaved, Snackbar.LENGTH_SHORT)
                        .show();

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Snackbar.make(getActivity().findViewById(android.R.id.content), R.string.error_noteSave, Snackbar.LENGTH_SHORT)
                        .show();

            }
        });
    }

    private void refreshTags()
    {
        mTags.removeAllTags();

        ArrayList<String> tags = new ArrayList<>(mNote.getTags());
        tags.add(getString(R.string.addTagLabel));

        mTags.setTags(tags);
    }

    private void injectAttachments()
    {
        int pos = 0;
        for(Attachment attch : mNote.getAttachments())
        {
            View root = LayoutInflater.from(getActivity()).inflate(R.layout.item_attachment, mAttachments, false);
            root.setTag(pos);

            root.setOnClickListener(mAttachDownloadListener);

            TextView name = (TextView) root.findViewById(R.id.name);
            name.setText(attch.getName());

            ImageButton delete = (ImageButton) root.findViewById(R.id.delete);
            delete.setOnClickListener(mAttachDeleteListener);
            delete.setTag(pos);

            mAttachments.addView(root);

            ++pos;
        }
    }

    private View.OnClickListener mAttachDownloadListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int pos = (int) v.getTag();
            Attachment attach = mNote.getAttachments().get(pos);
            Uri uri = DataManager.getInstance(getContext()).downloadNoteFileUri(attach);

            DownloadManager.Request request = new DownloadManager.Request(uri);
            request.setTitle(attach.getName());

            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, attach.getName());

            DownloadManager manager = (DownloadManager) getContext().getSystemService(Context.DOWNLOAD_SERVICE);
            manager.enqueue(request);
        }
    };

    private View.OnClickListener mAttachDeleteListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            final int pos = (int) v.getTag();

            Attachment attachment = mNote.getAttachments().get(pos);

            DataManager.getInstance(getActivity()).removeNoteFile(mNote, attachment, new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    if (!response.isSuccessful()) {
                        onFailure(call, null);
                        return;
                    }

                    mNote.getAttachments().remove(pos);
                    mAttachments.removeViewAt(pos);

                    if (getActivity() instanceof NoteHandler) {
                        NoteHandler handler = (NoteHandler) getActivity();
                        handler.onNoteSaved(mNote, false);
                    }
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    Snackbar.make(getActivity().findViewById(android.R.id.content), R.string.error_noteSave, Snackbar.LENGTH_SHORT)
                            .show();

                }
            });
        }
    };

    public interface NoteHandler
    {
        void onNoteSaved(Note note, boolean exit);

        void onNoteComments(Note note);
    }
}
