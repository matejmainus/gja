package gja.android.fragment;

import gja.android.DataManager;
import gja.android.R;
import gja.android.adapter.NoteAdapter;
import gja.android.model.Note;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NoteListFragment extends Fragment
{
    private NoteAdapter mAdapter = new NoteAdapter();

    @Bind(R.id.list)
    RecyclerView mList;

    @Bind(R.id.error_msg)
    TextView mErrorMsg;

    @Bind(R.id.list_refresh)
    SwipeRefreshLayout mListRefresh;

    @Bind(R.id.fab)
    FloatingActionButton mAddNoteButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(gja.android.R.layout.fragment_note_list, container, false);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);

        mAdapter.setItemClickListener(mListItemClickListener);

        mList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        mList.setAdapter(mAdapter);

        mListRefresh.setOnRefreshListener(mListRefreshListener);

        mAddNoteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getActivity() instanceof NoteListHandler) {
                    NoteListHandler handler = (NoteListHandler) getActivity();

                    mAdapter.addNote(handler.onAddNote());
                }
            }
        });

        loadNotes();

        showLoading();
    }

    public void selectItem(int pos)
    {
        Note note = mAdapter.getNote(pos);

        if(getActivity() instanceof NoteListHandler) {
            NoteListHandler handler = (NoteListHandler) getActivity();

            handler.onNoteSelected(note);
        }
    }

    private SwipeRefreshLayout.OnRefreshListener mListRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            loadNotes();
        }
    };

    private View.OnClickListener mListItemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int pos = mList.getChildAdapterPosition(v);

            selectItem(pos);
        }
    };

    public void loadNotes()
    {
        DataManager.getInstance(getActivity()).getNotes(new Callback<List<Note>>() {
            @Override
            public void onResponse(Call<List<Note>> call, Response<List<Note>> response) {

                if (!response.isSuccessful()) {
                    onFailure(call, null);
                    return;
                }

                mAdapter.setNotes(response.body());
                showContent();
            }

            @Override
            public void onFailure(Call<List<Note>> call, Throwable t) {
                showError(R.string.error_serverError);
            }
        });
    }

    public void showError(@StringRes int resourceId)
    {
        mListRefresh.setRefreshing(false);
        mErrorMsg.setVisibility(View.VISIBLE);
        mErrorMsg.setText(resourceId);
        mList.setVisibility(View.GONE);
    }

    public void showContent()
    {
        mListRefresh.setRefreshing(false);
        mErrorMsg.setVisibility(View.GONE);
        mList.setVisibility(View.VISIBLE);
    }

    public void showLoading()
    {
        mListRefresh.setRefreshing(true);
        mErrorMsg.setVisibility(View.GONE);
        mList.setVisibility(View.GONE);
    }

    public interface NoteListHandler
    {
        void onNoteSelected(Note note);

        Note onAddNote();
    }
}
