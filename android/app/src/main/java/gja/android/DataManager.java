package gja.android;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import gja.android.model.Attachment;
import gja.android.model.Comment;
import gja.android.model.Note;
import gja.android.model.User;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import okio.ByteString;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DataManager
{
    public static final String BASE_URI = "http://192.168.1.104:8080";
    //public static final String BASE_URI = "http://10.0.2.2:8080";

    public static final String API_URI = BASE_URI + "/api/";

    private static DataManager sInstance;

    private NevernoteService mService;
    private Context mContext;

    public static DataManager getInstance(Context context)
    {
        if(sInstance == null)
            sInstance = new DataManager(context);

        return sInstance;
    }

    private DataManager(Context context)
    {
        mContext = context;

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .addInterceptor(mAuthInterceptor)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_URI)
                //.baseUrl("http://147.229.178.242:8080/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        mService = retrofit.create(NevernoteService.class);
    }

    public void getNotes(Callback<List<Note>> callback)
    {
        mService.notes().enqueue(callback);
    }

    public void addNote(Note note, Callback<Void> callback) { mService.saveNote(note).enqueue(callback); }


    public void saveNote(Note note, Callback<Void> callback) { mService.saveNote(note).enqueue(callback); }

    public void removeNote(Note note, Callback<Void> callback)
    {
        mService.removeNote(note.getId()).enqueue(callback);
    }

    public void uploadNoteFile(InputStream is, Note note, Callback<Void> callback) throws IOException
    {
        RequestBody fileBody = RequestBody.create(MediaType.parse("image/*"), Utils.getBytes(is));

        mService.uploadNoteFile(fileBody, note.getId()).enqueue(callback);
    }

    public void removeNoteFile(Note note, Attachment attachment, Callback<Void> callback)
    {
        mService.removeNoteFile(note.getId(), attachment.getPath()).enqueue(callback);
    }

    public static Uri downloadNoteFileUri(Attachment attachment)
    {
        return Uri.parse(API_URI + "note/file?fileId=" + attachment.getPath());
    }

    public static Uri registerUserUri()
    {
        return Uri.parse(BASE_URI + "/register");
    }

    public void login(final User user, final Callback<Void> callback)
    {
        mService.login(user).enqueue(new Callback<Void>() {

            @Override
            public void onResponse(Call<Void> call, Response<Void> response)
            {
                if(!response.isSuccessful())
                {
                    onFailure(call, null);
                    return;
                }

                String authId = response.headers().get("Set-Cookie");

                if(authId != null)
                {
                    UserManager.getInstance(mContext).loginUser(user, authId);

                    if(callback != null)
                        callback.onResponse(call, response);
                }
                else
                {
                    onFailure(call, null);
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

                if(callback != null)
                    callback.onFailure(call, t);
            }
        });
    }

    public void postComent(Note note, Comment comment, Callback<Void> calback)
    {
        mService.postComent(note.getId(), comment.getText()).enqueue(calback);
    }

    public void getUserInfo(Callback<User> callback)
    {
        mService.userInfo().enqueue(callback);
    }

    private Interceptor mAuthInterceptor = new Interceptor()
    {
        @Override
        public okhttp3.Response intercept(Chain chain) throws IOException
        {
            String authToken = UserManager.getInstance(mContext).getAuthToken();
            if(authToken != null)
            {
                Request request = chain.request().newBuilder()
                        .addHeader("Cookie", authToken)
                        .build();

                return chain.proceed(request);
            }
            else
            {
                return chain.proceed(chain.request());
            }
        }
    };
}
