package gja.android;


import android.content.Context;
import android.content.SharedPreferences;

import gja.android.model.User;

public class UserManager {

    public static final String SHARED_PREF_ID = "userId";

    public static final String PREF_TOKEN = "token";

    private static UserManager sInstance = null;

    private SharedPreferences mSharedPreferences;
    private User mUser = null;

    private UserManager(Context context)
    {
        mSharedPreferences = context.getSharedPreferences(SHARED_PREF_ID, Context.MODE_PRIVATE);
    }

    public static UserManager getInstance(Context context) {

        if(sInstance == null)
            sInstance = new UserManager(context);

        return sInstance;
    }

    public void loginUser(User user, String token)
    {
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putString(PREF_TOKEN, token);

        editor.commit();
    }

    public void logout()
    {
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putString(PREF_TOKEN, null);

        editor.commit();

        mUser = null;
    }

    public void reloginUser(User user)
    {
        mUser = user;
    }

    public User getLoggerUser()
    {
        return mUser;
    }

    public String getAuthToken()
    {
        return mSharedPreferences.getString(PREF_TOKEN, null);
    }

}
