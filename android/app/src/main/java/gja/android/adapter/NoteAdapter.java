package gja.android.adapter;

import co.hkm.soltag.TagContainerLayout;
import gja.android.R;
import gja.android.Utils;
import gja.android.model.Note;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.ViewHolder>
{
    private ArrayList<Note> mNotes = new ArrayList<>();
    private View.OnClickListener mItemListener;

    public NoteAdapter()
    {}

    public void setNotes(List<Note> notes)
    {
        mNotes.clear();
        mNotes.addAll(notes);

        notifyDataSetChanged();
    }

    public void addNote(Note note)
    {
        mNotes.add(note);
    }

    public void removeNote(Note note)
    {
        mNotes.remove(note);
    }

    public void setItemClickListener(View.OnClickListener listener)
    {
        mItemListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(gja.android.R.layout.item_note, parent, false);
        rootView.setOnClickListener(mItemListener);

        ViewHolder vh = new ViewHolder(rootView);

        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Note note = getNote(position);

        holder.bind(note);
    }

    @Override
    public int getItemCount() {
        return mNotes.size();
    }

    public Note getNote(int position)
    {
        return mNotes.get(position);
    }

    static class ViewHolder extends RecyclerView.ViewHolder
    {
        @Bind(R.id.title)
        TextView mTitle;

        @Bind(R.id.tags)
        TagContainerLayout mTags;

        @Bind(R.id.attachments)
        TextView mAttachments;

        @Bind(R.id.comments)
        TextView mComments;

        public ViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        void bind(Note note)
        {
            Context ctx = itemView.getContext();

            mTitle.setText(note.getTitle());

            mTags.removeAllTags();
            mTags.setTags(note.getTags());

            int attach = note.getAttachments().size();

            mAttachments.setText(ctx.getString(R.string.attachmentsCount, attach));

            mComments.setText(ctx.getString(R.string.commentsCount, note.getComments().size()));
        }
    }
}
