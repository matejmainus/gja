
function onEditClick() {
    var _this = $(this);

    if(_this.prop('readonly') == true) {
        _this.prop('readonly', false);
    }
}

function onEditBlur() {
    var _this = $(this);

    if(_this.prop('readonly') != true) {
     _this.prop('readonly', true);
    }
}

function onEditTextClick() {
    var _this = $(this);
    var text = _this.parent().find("textarea");

    text.show();
    text.focus();
    text.val(_this.html());
    _this.hide();
}

function onEditTextBlur() {
    var _this = $(this);
    var text = _this.parent().find("div.textview");

    text.show();
    text.html(_this.val());
    _this.hide();
}

function createInputElem(_this, type) {
    var input = _this.find("input");

    input.prop('readonly', true);

    if(_this.attr("disabled") !== "disabled") {
        input.on("blur", onEditBlur);
        input.on("click", onEditClick);
    }
}

function createTextElem(_this) {
    var input = _this.find("textarea");

    input.hide();

    input.on("blur", onEditTextBlur);

    var textView = _this.find(".textview");

    textView.html(input.val());

    if(_this.attr("disabled") !== "disabled") {
        textView.on("click", onEditTextClick);
    }
}

function createLabelElem(_this) {
    var input = _this.find("input");

    var listElem = $("<div class='labels-inner' />");

    if(input.val()){
        var list = input.val().split(",");

        list.forEach(function(item) {
            var a = " <a href='#' class='glyphicon glyphicon-remove' onclick='removeLabel(this)'></a>";
            listElem.append("<span class='badge'>" + item + (_this.attr("disabled") !== "disabled" ? a : "") + "</span>");
        });
    }

    _this.append(listElem);

    if(_this.attr("disabled") !== "disabled") {
        _this.append("<input class='add-labels-input' onblur='addLabel(this)' placeholder='Add new label here' />");
    }
}

function removeLabel(_this) {
    _this = $(_this).parent();
    var parent = _this.parent();

    _this.remove();
    var newList = parent.find(".badge").map(function(){
        return $(this).text();
    }).toArray().join(",");
    parent.parent().find("input[type=hidden]").val(newList);
}

function addLabel(_this) {
    _this = $(_this);
    var value = _this.val();
    if(value) {
        _this.parent().find(".labels-inner").append("<span class='badge'>" + value + " <a href='#' class='glyphicon glyphicon-remove' onclick='removeLabel(this)'></a></span>");

        var input = _this.parent().find("input[type=hidden]");
        var inVal = input.val();
        input.val(inVal + (inVal ? "," : "") + value);
        _this.val("");
    }
}

function toggleShare() {
    var content = $(".mainContent");
    var shares = $(".shares");

    if(shares.is(":visible")) {
        shares.hide();
        content.removeClass("col-lg-9");
        content.addClass("col-lg-12");
    }
    else {
        shares.show();
        content.addClass("col-lg-9");
        content.removeClass("col-lg-12");
    }
}

function uploadFile() {
    var form = $("#attachmentForm");

    var formData = new FormData(form[0]);

    var noteId = form.find("input[type=hidden]").val();

    $.ajax({
        url: '/api/note/upload/',
        type: 'POST',

        success: function(data){
            //console.log("success", data);
            $("#attachmentTable tbody").append("<tr><td>" + data.name + "</td><td><a class=\"glyphicon glyphicon-download\" href=\"/api/note/file?fileId=" + data.path + "\" target=\"_blank\"></a></td><td><a href=\"#\" class=\"glyphicon glyphicon-trash\" onclick=\"removeFile(this, '" + data.path + "', '" + noteId + "')\"></a></td></tr>");
        },
        error: function(err){
            console.log("error", err);
        },

        data: formData,
        cache: false,
        contentType: false,
        processData: false
    });
}

function removeFile(_this, filePath) {
    $.ajax({
        url: '/api/note/removeAttachment/',
        type: 'POST',

        success: function(data){
            //console.log("success", data);
            $(_this).closest("tr").remove();
        },
        error: function(err){
            console.log("error", err);
        },

        data: "id=" + globals.selectedNoteId + "&path=" + filePath
    });
}

function logout() {
    $("#logoutForm").submit();
}

function shareNote(input) {
    _input = $(input);
    $.ajax({
        url: '/api/note/shareToUser/',
        type: 'GET',

        success: function(data){
            //console.log("success", data);

            if(data) {
                var select = "<select onchange=\"changePermissionsTo(this, '" + globals.selectedNoteId + "', '" + data.name + "')\">" +
                    "<option value='READ' selected='selected'>Read</option>" +
                    "<option value='COMMENT'>Comment</option>" +
                    "<option value='WRITE'>Write</option></select>";
                _input.closest("ul").append("<li class='list-group-item'><span>" + data.name + "</span>" + select + "<a href='#' onclick=\"stopSharingNote(this, '" + globals.selectedNoteId + "', '" + data.userId + "')\" class='pull-right glyphicon glyphicon-remove'></a></li>");
            }
            _input.val("");
        },
        error: function(err){
            console.log("error", err);
        },

        data: "noteId=" + globals.selectedNoteId + "&userName=" + _input.val() + "&perm=READ"
    });
}

function changePermissionsTo(input, userName) {
    _input = $(input);
    $.ajax({
        url: '/api/note/shareToUser/',
        type: 'GET',

        success: function(data){
            //console.log("success", data);
        },
        error: function(err){
            console.log("error", err);
        },

        data: "noteId=" + globals.selectedNoteId + "&userName=" + userName + "&perm=" + _input.val()
    });
}

function stopSharingNote(elem, userId) {
    $.ajax({
        url: '/api/note/stopSharingToUser/',
        type: 'GET',

        success: function(data){
            //console.log("success", data);
            $(elem).closest("li").remove();
        },
        error: function(err){
            console.log("error", err);
        },

        data: "noteId=" + globals.selectedNoteId + "&userId=" + userId
    });
}

function addComment(elem) {
    _elem = $(elem);
    $.ajax({
        url: '/api/note/comment/',
        type: 'GET',

        success: function(data){
            //console.log("success", data);
            if(data) {
                _elem.closest(".comments").find(".comments-body").append("<div class='comment'><div class='header'><span class='name'>" + data.name + "</span></div>" +
                    "<div class='text'>" + data.text + "</div></div>");
                _elem.parent().find("textarea").val("");
            }
        },
        error: function(err){
            console.log("error", err);
        },

        data: "noteId=" + globals.selectedNoteId + "&text=" + _elem.parent().find("textarea").val()
    });
}

$(function() {
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });

    $("[permCheck]").each(function() {
        var _this = $(this);
        var perm = _this.attr("permCheck") || "WRITE";

        if(perm.split(",").indexOf(globals.permission) === -1) {
            switch(_this.prop("tagName").toLowerCase()) {
                case 'a': case 'button': case 'form':
                    _this.hide();
                    break;
                case 'div': case 'h1':
                    if(_this.attr("editable")) {
                        _this.attr("disabled", "disabled");
                    }
                    else {
                        _this.hide();
                    }
                    break;
                case 'input': case 'textarea':
                    _this.prop('disabled', true);
                    break;
            }
        }
    });

    $("[editable]").each(function() {
        var _this = $(this);
        var type = _this.attr("editable") || "text";


        if(type == "textarea") {
            createTextElem(_this, type);
        }
        else if(type == "label") {
            createLabelElem(_this);
        }
        else {
            createInputElem(_this);
        }
    });
});
