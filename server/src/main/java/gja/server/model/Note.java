package gja.server.model;

import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Note
{
    public enum Permissions {
        READ, COMMENT, WRITE
    }

    public static class Attachment {
        public String name;
        public String path;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }
    }

    public static class Share {
        public String userId;
        public String name;
        public Permissions permission;

        public String getUserId() { return userId; }

        public void setUserId(String userId) { this.userId = userId; }

        public String getName() { return name; }

        public void setName(String name) { this.name = name; }

        public Permissions getPermission() { return permission; }

        public void setPermission(Permissions permission) { this.permission = permission; }
    }

    public static class Comment {
        public String userId;
        public String name;
        public Date time;
        public String text;

        public String getUserId() { return userId; }

        public void setUserId(String userId) { this.userId = userId; }

        public String getName() { return name; }

        public void setName(String name) { this.name = name; }

        public Date getTime() { return time; }

        public void setTime(Date time) { this.time = time; }

        public String getText() { return text; }

        public void setText(String text) { this.text = text; }
    }

    @Id
    private String id;

    private String title;

    private String text;

    private List<String> tags = new ArrayList<>();

    private List<Attachment> attachments = new ArrayList<>();

    private String userId;

    private List<Share> shares = new ArrayList<>();

    private List<Comment> comments = new ArrayList<>();


    public String getId() { return id; }
    public void setId(String id) { this.id = id; }

    public String getTitle() { return title; }
    public void setTitle(String title) { this.title = title; }

    public String getText() { return text; }
    public void setText(String text) { this.text = text; }

    public List<String> getTags() { return tags; }
    public void setTags(List<String> list) { this.tags = list; }
    public void addTag(String tag) { this.tags.add(tag); }


    public List<Attachment> getAttachments() { return attachments; }
    public void setAttachments(List<Attachment> list) { this.attachments = list; }

    public void removeAttachmentByPath(String path) {
        int idx = 0;
        for(Attachment att : attachments) {
            if(att.path.equals(path)) {
                break;
            }
            idx++;
        }
        attachments.remove(idx);
    }

    public String getUserId() {
        return userId;
    }
    public void setUserId(String userId) { this.userId = userId; }

    public List<Share> getShares() { return shares; }
    public void setShares(List<Share> shares) { this.shares = shares; }

    public List<Comment> getComments() { return comments; }
    public void setComments(List<Comment> comments) { this.comments = comments; }
}
