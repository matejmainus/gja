package gja.server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

import gja.server.model.Note;
import gja.server.repository.NoteRepository;
import gja.server.repository.UserRepository;

@Controller
public class NoteController
{
    @Autowired
    NoteRepository mNoteRepository;

    @Autowired
    UserRepository mUserRepository;

    @RequestMapping("/")
    public String home() {
        return "redirect:/notes";
    }

    @RequestMapping("/notes")
    public String notes(@RequestParam(defaultValue = "", required = false) String noteId, @RequestParam(defaultValue = "", required = false) String search, Model model) {

        List<Note> notes = mNoteRepository.notes(search);

        Note selectedNote = null;
        Note.Permissions perm = Note.Permissions.READ;
        if(!noteId.isEmpty()) {
            selectedNote = mNoteRepository.findById(noteId);

            if(selectedNote.getUserId().equals(mUserRepository.getCurrentUser().getId())) {
                perm = Note.Permissions.WRITE;
            }
            for (Note.Share share : selectedNote.getShares()) {
                if(share.getUserId().equals(mUserRepository.getCurrentUser().getId())) {
                    perm = share.getPermission();
                    break;
                }
            }
        }

        model.addAttribute("notes", notes);
        model.addAttribute("search", search);
        model.addAttribute("selectedNote", selectedNote);
        model.addAttribute("permission", perm.toString());
        model.addAttribute("currentUserId", mUserRepository.getCurrentUser().getId());

        return "index";
    }

    @RequestMapping("/notes/new")
    public String newNote(Model model) {
        Note note = mNoteRepository.create();

        return "redirect:/notes?noteId="+note.getId();
    }

    @RequestMapping(value = "/notes/save", method = RequestMethod.POST)
    public String saveNote(@ModelAttribute Note note) {
        mNoteRepository.update(note);

        return "redirect:/notes?noteId="+note.getId();
    }

    @RequestMapping(value = "/notes/delete")
    public String deleteNote(@RequestParam(required = true) String noteId) {
        mNoteRepository.remove(noteId);

        return "redirect:/notes";
    }

}
