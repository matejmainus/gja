package gja.server.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import gja.server.model.Note;
import gja.server.model.User;

@Service
public class NoteRepository
{
	@Autowired
	MongoOperations mongo;

	@Autowired
	UserRepository userRepo;

	public void insert(Note note)
	{
		mongo.insert(note);
	}

	public Note findById(String id)
	{
		try {
			return mongo.find(new Query(new Criteria().andOperator(
					Criteria.where("_id").is(id),
					new Criteria().orOperator(
							Criteria.where("userId").is(userRepo.getCurrentUser().getId()),
							Criteria.where("shares").elemMatch(new Criteria().is(userRepo.getCurrentUser().getId()))
					)
			)), Note.class).get(0);
		}
		catch (IndexOutOfBoundsException ex) {
			return null;
		}
	}

	public List<Note> notes()
	{
		return notes("");
	}

	public List<Note> notes(String search)
	{
		Pattern regexp = Pattern.compile(search, Pattern.CASE_INSENSITIVE);

		Query q = new Query(
			new Criteria().andOperator(
				new Criteria().orOperator(
					Criteria.where("text").regex(regexp),
					Criteria.where("title").regex(regexp),
					Criteria.where("tags").elemMatch(new Criteria().regex(regexp))
				),
				new Criteria().orOperator(
					Criteria.where("userId").is(userRepo.getCurrentUser().getId()),
					Criteria.where("shares").elemMatch(new Criteria().is(userRepo.getCurrentUser().getId()))
				)
			)
		);
		return mongo.find(q, Note.class);
	}

	public Note create() {
		Note note = new Note();
		note.setTitle("New Note");

		note.setUserId(userRepo.getCurrentUser().getId());

		mongo.insert(note);
		return note;
	}

	public void update(Note note) {
		Update updt = new Update();
		updt.set("title", note.getTitle());
		updt.set("text", note.getText());
		updt.set("tags", note.getTags());
		mongo.updateFirst(new Query(Criteria.where("_id").is(note.getId())), updt, Note.class);
	}

	public void save(Note note) {

		note.setUserId(userRepo.getCurrentUser().getId());

		mongo.save(note);
	}

	public void remove(String noteId) {
		mongo.remove(new Query(Criteria.where("_id").is(noteId)), Note.class);
	}

	public void remove(Note note) { mongo.remove(note); }

	public Note.Attachment getAttachmentByPath(String filePath) {
		Note note = mongo.find(new Query(Criteria.where("attachments").elemMatch(Criteria.where("path").is(filePath))), Note.class).get(0);
		for (Note.Attachment it : note.getAttachments()) {
			if(it.path.equals(filePath)) {
				return it;
			}
		}

		return null;
	}

	public Note.Share shareNoteToUser(String noteId, String userName, Note.Permissions perm) {
		Note note = this.findById(noteId);

		if(note == null) return null;

		User user;

		try {
			user = mongo.find(new Query(Criteria.where("name").is(userName)), User.class).get(0);
		}
		catch (IndexOutOfBoundsException ex) {
			return null;
		}

		if(user.getId().equals(note.getUserId())) return null;

		Note.Share share = null;
		for (Note.Share sh : note.getShares()) {
			if(sh.getUserId().equals(user.getId())) {
				System.out.println("way1");
				sh.setPermission(perm);
				share = sh;
				break;
			}
		}
		if(share == null) {
			System.out.println("way2");
			share = new Note.Share();
			share.setUserId(user.getId());
			share.setName(user.getName());
			share.setPermission(perm);
			note.getShares().add(share);
		}

		this.save(note);

		return share;
	}

	public boolean stopSharingNoteToUser(String noteId, String userId) {
		Note note = this.findById(noteId);

		if(note == null) return false;

		for (Note.Share share : note.getShares()) {
			if(share.getUserId().equals(userId)) {
				note.getShares().remove(share);
				break;
			}
		}

		this.save(note);

		return true;
	}

	public Note.Comment addComment(String text, String noteId) {
		Note note = this.findById(noteId);

		if(note == null) return null;

		User user;

		try {
			user = mongo.find(new Query(Criteria.where("_id").is(userRepo.getCurrentUser().getId())), User.class).get(0);
		}
		catch (IndexOutOfBoundsException ex) {
			return null;
		}

		Note.Comment comm = new Note.Comment();
		comm.setUserId(user.getId());
		comm.setName(user.getName());
		comm.setTime(new Date());
		comm.setText(text);
		note.getComments().add(comm);

		this.save(note);

		return comm;
	}
}
