package gja.server.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import gja.server.model.User;

@Service
public class UserRepository
{
	@Autowired
	MongoOperations mongo;

	public User getCurrentUser() {
		return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}

	public User findUserByName(String name) {
		return mongo.find(new Query(Criteria.where("name").is(name)), User.class).get(0);
	}

	public void addUser(User user) throws Exception {
		try {
			this.findUserByName(user.getName());
			throw new Exception("Username already taken! Ha Ha");
		}
		catch (IndexOutOfBoundsException ex) {
			mongo.insert(user);
		}

	}
}
