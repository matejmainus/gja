package gja.server.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import gja.server.SecUserDetailsService;
import gja.server.model.User;
import gja.server.repository.UserRepository;

@RestController
@RequestMapping("/api/user")
public class UserAPIController
{
	@Autowired
	SecUserDetailsService mUserService;

	@Resource(name = "authenticationManager")
	AuthenticationManager mAuthManager;

	@Autowired
	UserRepository mUserRepository;

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public void login(@RequestBody User user, HttpServletRequest request, HttpServletResponse response)
	{
		UserDetails userDetails = mUserService.loadUserByUsername(user.getName());

		if((userDetails != null) && userDetails.getPassword().equals(user.getPassword()))
		{
			UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails.getUsername(), userDetails.getPassword());
			authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

			SecurityContextHolder.getContext()
					.setAuthentication(mAuthManager.authenticate(authentication));
		}
		else
		{
			response.setStatus(HttpStatus.FORBIDDEN.value());
		}
	}

	@RequestMapping(value = "/info", method = RequestMethod.GET)
	public User getInfo()
	{
		User user = mUserRepository.getCurrentUser();
		user.setPassword(null);

		return user;
	}
}
