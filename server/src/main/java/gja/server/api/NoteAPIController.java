package gja.server.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;

import gja.server.Application;
import gja.server.model.Note;
import gja.server.repository.NoteRepository;

@RestController
@RequestMapping("/api/note")
public class NoteAPIController
{
	@Autowired
	private NoteRepository mNoteRepository;

	@RequestMapping("/list")
	public List<Note> getNotes(){
		List<Note> notes = mNoteRepository.notes();
		return notes;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/save")
	public void saveNote(@RequestBody Note note)
	{
		mNoteRepository.save(note);
	}

	@RequestMapping(value = "/remove")
	public void removeNote(@RequestParam(value="id", required=true) String id)
	{
		if(!id.isEmpty())
			mNoteRepository.remove(id);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/add")
	public void addNote(@RequestBody Note note){
		mNoteRepository.insert(note);
	}


	@RequestMapping(method = RequestMethod.POST, value = "/upload")
	public Note.Attachment handleFileUpload(@RequestParam("id") String id,
								   @RequestParam("file") MultipartFile file) {

		if (!file.isEmpty()) {
			try {
				String newName = UUID.randomUUID().toString();

				String extension = "";

				if(extension.lastIndexOf(".") >= 0)
					extension = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));

				String fileName = newName + extension;

				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(new File(Application.AttachmentRootPath + File.separator + fileName)));
				FileCopyUtils.copy(file.getInputStream(), stream);
				stream.close();

				Note.Attachment att = new Note.Attachment();
				att.name = file.getOriginalFilename();
				att.path = fileName;
				/*redirectAttributes.addFlashAttribute("message",
						"You successfully uploaded " + name + "!");*/

				Note note = mNoteRepository.findById(id);
				note.getAttachments().add(att);
				mNoteRepository.save(note);

				return att;
			}
			catch (Exception e) {
				e.printStackTrace();

				/*redirectAttributes.addFlashAttribute("message",
						"You failed to upload " + name + " => " + e.getMessage());*/
			}
		}
		else {
			/*redirectAttributes.addFlashAttribute("message",
					"You failed to upload " + name + " because the file was empty");*/
		}

		return null;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/removeAttachment")
	public void handleRemoveFile(@RequestParam("id") String id,
											@RequestParam("path") String filePath) {

		File f = new File(Application.AttachmentRootPath + File.separator + filePath);
		if(!f.delete()) {
			throw new RuntimeException("Can't delete file.");
		}

		Note note = mNoteRepository.findById(id);
		note.removeAttachmentByPath(filePath);
		mNoteRepository.save(note);
	}

	@RequestMapping(value = "/file", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public FileSystemResource getFile(@RequestParam("fileId") String filePath, HttpServletResponse response) {
		Note.Attachment att = mNoteRepository.getAttachmentByPath(filePath);
		response.setHeader("Content-Disposition", "attachment; filename=" + att.name);
		return new FileSystemResource(new File(Application.AttachmentRootPath + File.separator + filePath));
	}

	@RequestMapping(value = "/shareToUser", method = RequestMethod.GET)
	public Note.Share shareToUser(@RequestParam("noteId") String noteId,
							@RequestParam("userName") String userName,
							@RequestParam("perm") String perm) {

		return mNoteRepository.shareNoteToUser(noteId, userName, Note.Permissions.valueOf(perm));
	}

	@RequestMapping(value = "/stopSharingToUser", method = RequestMethod.GET)
	public boolean stopSharingToUser(@RequestParam("noteId") String noteId,
							   @RequestParam("userId") String userId) {

		return mNoteRepository.stopSharingNoteToUser(noteId, userId);
	}

	@RequestMapping(value = "/comment", method = RequestMethod.GET)
	public Note.Comment addComment(@RequestParam("noteId") String noteId,
									 @RequestParam("text") String text) {

		return mNoteRepository.addComment(text, noteId);
	}

}
