package gja.server;

import com.mongodb.MongoClient;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;

@Configuration
@EnableAutoConfiguration
public class AppConfig
{

	public @Bean
	MongoDbFactory mongoDbFactory() throws Exception {
		MongoClient client = new MongoClient("localhost");
		return new SimpleMongoDbFactory(client, "gja");
	}

	public @Bean
	MongoTemplate mongoTemplate() throws Exception {
		return new MongoTemplate(mongoDbFactory());
	}
}
