package gja.server;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import java.io.File;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }


    public static String AttachmentRootPath = "upload-dir";

    @Bean
    CommandLineRunner init() {
        return new CommandLineRunner() {
            @Override
            public void run(String... args) throws Exception {
                new File(AttachmentRootPath).mkdir();
            }
        };
    }
}
